\chapter{Introduction}

Linear Diophantine Systems (LDS) are systems of linear equations and inequalities with non-negative integer solutions. They are an important mathematical tool in various fields such as operations research, computer science, and engineering. The efficient and effective solution of LDS is a well-known problem with a long history of research. In this thesis, we present a new algorithm for solving LDS, called Polyhedral Omega, that combines methods from partition analysis with methods from polyhedral geometry. The algorithm is implemented in Julia programming language, which allows for high performance and ease of use. \ref{fig:pol_omeg_analogy}.

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=1\textwidth]{Imgs/polyhedral_omega_graph.png}
    \caption{\label{fig:pol_omeg_analogy}Analogy of Polyhedral Omega\cite{polyhedral_omega}}
\end{figure}

\section{Linear Diophantine Systems}

 Let $A \in \mathbb{Z}^{m\times n}$ be an integer matrix and let $b \in \mathbb{Z}^{m}$ be an integer vector. The non-negative solution system consisting of any combination of equations and inequalities that can be represented as $Ax \geq b$, such systems are called  \textit{linear Diophantine system} (LDS). Linear Diophantine systems (LDS) are important in both practice and theory. They are used in fields such as operations research and combinatorial optimization, and the Integer Programming (IP)\footnote{An integer programming problem is a mathematical optimization or feasibility program in which some or all of the variables are restricted to be integers.} problem, which is about finding the optimal solution of an LDS, plays a pivotal role in these fields. The set of solutions to a Linear Diophantine System can be infinite. One way to represent such infinite sets of solutions is multivariate rational functions.

\section{Rational Functions}

A rational function is a function that can be written in the form of

$$
f(x) = \frac{P(x)}{Q(x)}
$$

where P(x) and Q(x) are polynomials, and Q(x) is not the zero polynomial. It can be defined in the complex or real numbers field. The study of rational functions is important in many areas such as complex analysis, algebraic geometry, and control theory. 
For example,

$$
f(x) = \frac{x^2 + 2x + 1}{x - 1}
$$

is a rational function where $P(x) = x^2 + 2x + 1 $ and $Q(x) = x - 1$ are polynomials.

\subsection{Multivariate Rational Functions}
A multivariate rational function is a function that can be represented by a ratio of two polynomials in multiple variables. It can be written in the form of

$$
f(x_1,x_2,x_3,..,x_n) = \frac{P(x_1,x_2,x_3,..,x_n)}{Q(x_1,x_2,x_3,..,x_n)}
$$

where $P(x_1,x_2,x_3,..,x_n)$ and $Q(x_1,x_2,x_3,..,x_n)$ are polynomials in multiple variables and $Q(x_1,x_2,x_3,..,x_n)$ is not the zero polynomial.

For example,

$$
f(x,y) = \frac{x^2 + y^2 + 2x + 2y + 1}{x - y}
$$

is a multivariate rational function where $P(x,y) = x^2 + y^2 + 2x + 2y + 1$ and $Q(x,y) = x - y$ are polynomials in two variables x and y. \\
We identify a vector $x \in \mathbb{Z}^n$ with the monomial $z^x := z_1^{x_1} \cdot z_2^{x_2} \cdot\cdot\cdot z_n^{x_n}$ in n variables.

\section{Geometric Series Expansion}
A geometric series is a series of numbers in which each term is the product of the preceding term and a constant. It can be represented in the form of

$$
\sum_{n=0}^{\infty} ar^n = a + ar + ar^2 + ar^3 + ...
$$

where a is the first term of the series and r is the common ratio. If the absolute value of r is less than one, the series converges to the sum:

$$
\frac{a}{1-r}
$$

By using the geometric series expansion formula, the set of solutions $z^x := z_1^{x_1} \cdot z_2^{x_2} \cdot\cdot\cdot z_n^{x_n}$ by the rational functions as below,
\\
\begin{center}
    $\ffrac{1}{1 - z^{(1,1)}}$ = $\ffrac{1}{1 - z_1z_2}$ = $\sum_{i = 0}^\infty z^{(0,0)}$ = $z^{(0,0)}$ + $z^{(1,1)}$ + $z^{(2,2)}$ $\cdot\cdot\cdot$. 
\end{center}

The sets of non-negative integer vectors ($S \subset \mathbb{Z}_{\geq 0}^n$) are represented by multivariate functions as follows,
\begin{center}
    $\phi_S(z)$ = $\sum_{x\in S} z^x$
\end{center}
which means the coefficient of $z^x$ in $\phi_S$ is 1 if $x \in S$ and 0 if $x \not\in S$. If $S$ is the set of solutions to a Linear Diophantine System, them $\phi_S$ is always a rational function $\rho_S$ which is the rational function that is seeken while solving given Linear Diophantine System. 

\section{Generating Functions}
Generating functions are a powerful tool used in combinatorics, number theory, and other areas of mathematics. They are formal power series that encode combinatorial information about sequences of numbers. They provide a way to express a sequence of numbers as the coefficients of a single function, making it easier to analyze and manipulate the sequence. A generating function is usually denoted by a capital letter such as G(x) or F(x) and it is a function of a variable x.

A common way to define a generating function is by using the ordinary generating function (OGF) of a sequence. An OGF is a generating function of the form

$$
G(x) = \sum_{n=0}^{\infty} a_n x^n
$$

where $a_n$ is the n-th term of the sequence and x is a variable. The coefficients of the function G(x) are the terms of the sequence, and they can be found by expanding the function in powers of x.

\subsection{Crude Generating Functions}
A crude generating function (CGF) is a generating function that encodes the information of a sequence of integers without specifying the exact values of the terms of the sequence. It is represented by a formal power series of the form

$$
F(x) = \sum_{n=0}^{\infty} x^n
$$

where x is a variable and the summation is taken over all the non-negative integers.

A crude generating function is not a true generating function, as it does not have a specific sequence of integers as coefficients and it is not a convergent series. However, it is a useful tool for counting the number of ways in which a certain problem can be solved, and it can be used to derive more specific generating functions.

Crude generating functions are a useful tool to count the number of ways a certain problem can be solved, but they are not specific enough to give the exact values of the terms of the sequence. They can be useful to derive more specific generating functions.

\section{Indicator Function}

For any set $S \in \mathbb{R}^n$, we define the \emph{indicator function} $[S] : \mathbb{R}^n \rightarrow \mathbb{R}$ by,

\begin{center}
    \begin{equation}
    [S](x) = 
    \left\{
        \begin{array}{lr}
            1 & if x\in S\\
            0 & if x\not\in S\\
        \end{array}
    \right.
    \end{equation}
\end{center}

Indicator functions form a vector space. In particular, addition of indicator functions is defined pointwise, as is multiplication with a scalar. For a symbolic cone $C = (V,q,o)$ we will write $[C]$ to denote the indicator function $l_{\mathbb{R}}^o (V;q)$ of the associated simplicial cone.

\section{Partition Analysis}

Partition analysis is a method for solving Linear Diophantine Systems (LDS) that is based on iteratively applying the Omega operator to the given system of equations and inequalities. The Omega operator is a linear operator that maps a polynomial in multiple variables to a polynomial in one variable. The key idea of partition analysis is to iteratively apply the Omega operator to the given system until a univariate polynomial is obtained, and then use algebraic techniques to find the solutions of this univariate polynomial.

One of the key concepts in partition analysis is the MacMahon's iterative approach. It is based on the repeated application of the Omega operator to the given system until a univariate polynomial is obtained. The MacMahon's iterative approach can be represented by the following formula:

$$
\Omega^k(F(x_1,x_2,..,x_n)) = \Omega(\Omega^{k-1}(F(x_1,x_2,..,x_n)))
$$

where $F(x_1,x_2,..,x_n)$ is the polynomial representation of the given system of equations and inequalities, k is the number of iterations, and the exponent k represents the number of times the Omega operator is applied to $F(x_1,x_2,..,x_n)$. \\
When we consider a Linear Diophantine inequality
\begin{center}
    $2x_1 + 3x_2 - 5x_3 \geq 4$.
\end{center}

To solve this system, MacMahon introduces the \emph{Omega operator} $\Omega_\geq$ defined on rational functions in $\mathbb{K}(x_1, x_2, x_3)(\lambda)$ in terms of their series expansion by\\

\begin{center}
    $\Omega_\geq \sum_{s = -\infty}^{+\infty}{c_s\lambda^s} = \sum_{s = 0}^{+\infty}c_s$ 
\end{center}

where $c_s\in \mathbb{K}(x_1, x_2, x_3)$ for all s. In other words, $\Omega_\geq$ acts by truncating all terms in the series expansion with negative $\lambda$ exponent and dropping the variable $\lambda$. We can write how Omega Operator is used over the generating function $\phi_S$ of the set of solutions S as follows,

\begin{center}
    $\phi_S (z) = \Omega_\geq \ffrac{\lambda^{-4}}{(1 - z_1\lambda^2)(1-z_2\lambda^3)(1-z_3\lambda^{-5})}$
\end{center}

The expression on the right-hand side of this equation is known as \emph{crude generating function}. Partition analysis is about developing calculi of explicit formulas for evaluating
the Omega operator when applied to rational functions of a certain form.



\section{Polyhedral Geometry}

 Polyhedral Geometry provides a rich set of tools for analyzing and manipulating 3D shapes mathematically. 
One of the key concepts in polyhedral geometry is the concept of a polyhedron.

\subsection{Polytope}
A polytope\ref{fig:polytope} is a generalization of a polyhedron to any number of dimensions. It is a geometric shape that can be represented by a set of vertices and edges in any number of dimensions. The vertices are the points that define the shape, and the edges are the lines that connect the vertices.
\begin{figure}[!htbp]
    \centering
    \includegraphics[width=0.5\textwidth]{Imgs/polytope.png}
    \caption{\label{fig:polytope}Examples of Polytopes\cite{polytope_example_pic}}
\end{figure}


In two dimensions, a polytope is called a polygon\ref{fig:polygon}. A polygon is a 2-dimensional geometric shape that is made up of a collection of straight edges. For example, a triangle is a 3-sided polygon and a square is a 4-sided polygon.

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=0.4\textwidth]{Imgs/polygon-1.png}
    \caption{\label{fig:polygon}Examples of Polygon\cite{polygon_example_pic}}
\end{figure}
\newpage

In three dimensions, a polytope is called a polyhedron\ref{fig:polyhedrons}. A polyhedron is a 3-dimensional geometric shape that is made up of a collection of flat faces, edges, and vertices. \\
In higher dimensions, a polytope can be called a polychoron\ref{fig:polychoron}. A 4-dimensional polytope is called a polychoron, and it can be represented by a set of vertices, edges, faces and cells.

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=0.5\textwidth]{Imgs/uniform_polychoron.png}
    \caption{\label{fig:polychoron}Example of uniform Polychoron\cite{polychoron_example_pic}}
\end{figure}

Polytopes are not only important for their geometric properties but also for their combinatorial and algebraic properties, which are important in different fields of mathematics and physics such as computer graphics, optimization, and physics.


\subsection{Polyhedron}
Polyhedron\ref{fig:polyhedrons}. is a 3-dimensional geometric shape that is made up of a collection of flat faces, edges, and vertices. Polyhedra can have various shapes and sizes, and can be found in many different forms in nature and in human-made structures.

One example of a polyhedron is the cube. A cube is a polyhedron with six square faces, eight vertices, and twelve edges. It can be represented mathematically as:

$$
ABCD, ABFE, BCGF, CDHG, DAHE, EFGH
$$

Where ABCD represents the four vertices of the square base, and the edges are represented by the lines connecting the vertices.
\begin{figure}[!htbp]
    \centering
    \includegraphics[width=1\textwidth]{Imgs/polyhedrons.png}
    \caption{\label{fig:polyhedrons}Examples of Polyhedrons\cite{polyhedron_example_pic}}
\end{figure}

Another important concept in polyhedral geometry is the concept of a polytope. 


\section{Cones}

 A cone can be defined as the set of all the points in 3D space that are at a fixed distance from a given point, called the vertex of the cone, and lie on a plane that passes through the vertex and is parallel to a given vector, called the axis of the cone.

A cone can be defined as follows:

$${(x,y,z) \in \mathbb{R}^3 \ | \ (x-x_0)^2 + (y-y_0)^2 = z^2}$$

where $(x_0, y_0, z_0)$ is the vertex of the cone and $z$ is the axis of the cone.

\subsection{Polyhedral Cone}
A polyhedral cone\ref{fig:polyhedral_cone} can be defined as the set of all the points in 3D space that are at a fixed distance from a given point, called the vertex of the cone, and lie on a plane that passes through the vertex and is parallel to a given vector, called the axis of the cone, and these points are on the faces of a given polyhedron.

In mathematical notation, a polyhedral cone can be defined as:

$${(x,y,z) \in \mathbb{R}^3 \ | \ (x-x_0)^2 + (y-y_0)^2 = z^2, (x,y) \in P}$$

Where $(x_0, y_0, z_0)$ is the vertex of the cone, $z$ is the axis of the cone, and P is a given polyhedron.

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=0.5\textwidth]{Imgs/Polyhedral-cone.png}
    \caption{\label{fig:polyhedral_cone}Example of a Polyhedral Cone\cite{polyhedral_Cone}}
\end{figure}

\subsection{Simplicial Polyhedral Cone}

A Simplicial Polyhedral Cone\ref{fig:simplicial_polyhedral_cone} is a special type of polyhedral cone, where the base of the cone is a simplex. A simplex is a geometric shape that is made up of a set of points in any number of dimensions where the points are connected by edges such that the shape is the smallest possible convex set. In 2D, a simplex is a triangle and in 3D it's a tetrahedron.

A Simplicial Polyhedral Cone can be defined as the set of all the points in 3D space that are at a fixed distance from a given point, called the vertex of the cone, and lie on a plane that passes through the vertex and is parallel to a given vector, called the axis of the cone, and these points are on the faces of a given simplex.

In mathematical notation, a Simplicial Polyhedral Cone can be defined as:

$${(x,y,z) \in \mathbb{R}^3 \ | \ (x-x_0)^2 + (y-y_0)^2 = z^2, (x,y) \in S}$$

Where $(x_0, y_0, z_0)$ is the vertex of the cone, $z$ is the axis of the cone, and S is a given simplex.

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=0.6\textwidth]{Imgs/simplicial_polyhedral_cone.png}
    \caption{\label{fig:simplicial_polyhedral_cone}Example of a Simplicial Polyhedral Cone\cite{simplicial_polyhedral_cone}}
\end{figure}






\section{Contribution}

In this thesis, I present an efficient and effective new algorithm for solving linear Diophantine systems, named Polyhedral Omega. This algorithm combines methods from partition analysis and polyhedral geometry and provides a detailed and illustrated study of these two fields from a unified point of view. The key contribution of this thesis is the implementation of the Polyhedral Omega algorithm in Julia programming language. The implementation of this algorithm in Julia provides a number of advantages over other programming languages, such as its high-performance capabilities, easy-to-read syntax, and a rich ecosystem of mathematical libraries that can be easily integrated into the algorithm.\\~\\

By providing an implementation of the Polyhedral Omega algorithm in Julia, I aim to make this powerful tool more accessible to a wider community of researchers, practitioners, and students. This implementation will allow users to easily test and experiment with the algorithm on their own systems and datasets. Additionally, by leveraging the capabilities of Julia, the algorithm can be further optimized and scaled to handle larger and more complex problems.\\~\\

In conclusion, my contribution to this research is the implementation of the Polyhedral Omega algorithm in Julia programming language, which makes this powerful tool more accessible to a wider community of users and enables further optimization and scalability to handle larger and more complex problems.


%%\ref{tab:widgets} \cite{One, Two, Three}.

%%\begin{table}[!htbp]
%%    \centering
%%    \caption{\label{tab:widgets}An example table.}
%%    \begin{tabular}{l|r}
%%        Item & Quantity \\\hline
%%        Widgets & 45 \\
%%        Gadgets & 13
%%    \end{tabular}
%% \end{table}
\chapter{JULIA IMPLEMENTATION}

\section{Smith Normal Form}

The Smith normal form (SNF) is a decomposition of an integer matrix into a product of three simpler matrices. The diagonal entries of one matrix, can be denoted as d\_1, d\_2, ..., d\_n, are called the invariant factors of the matrix, and are unique up to the order and the sign of the diagonal entries. The SNF is unique up to the order of the invariant factors and the sign of the diagonal entries. The SNF is also useful as a tool to determine the rank and the determinant of the matrix.

In this implementation, the function takes an integer matrix as input and returns a tuple containing the SNF S, the matrices F and T used in the decomposition, and the rank of the matrix. An example of how to use the function is as follows:

\begin{lstlisting}
include("SmithNormalForm.jl")
M = [1 2; 3 4 ]
smith(M) #Output of this function gives F,S and T matrices
\end{lstlisting}

\section{Helper Functions}

In this implementation, helper functions are functions that is defined to perform a specific task, and is intended to be used by one or more other functions in a program. Helper functions are commonly used to break down complex tasks into smaller, more manageable subtasks, making the code more readable and maintainable. Thats why in this impelementation, some helper functions are used. 

\subsection{List of Helper Functions}
\begin{itemize}
    \item msv(s,v) : Multiplies all entries of the vector v with a scalar s.
    \item svv(v,w) : Sums two vectors v and w.
    \item vector\_is\_forward(v) : Checks if a vector is forward, which means first non-zero.
    \item postmult(v) : Takes a rational vector v and returns a positive multiple of v that is integer.
    \item is\_integral(v) : Checks if a vector v is containing a vector that has integer elements.
    \item prim\_v(v) : Takes an integer vector and returns corresponding primitive integer vector.
    \item prim(v\_list) : Takes a vector of vectors and applies prim\_v to each of its elements.
    \item fract\_simple(v) : Takes a vector and returns vector of only fractional parts.
    \item sort\_tuplized(i) : Takes inputs and sort them according to first item.
\end{itemize}

\section{SymbolicCones Struct}

A symbolic cone is a mathematical object that can be represented as the solution set of a set of homogeneous linear inequalities, also known as conic constraints. These constraints are defined by a linear operator, called the cone generator, acting on a vector space. The resulting symbolic cone is a subset of the vector space, and is closed under addition and scalar multiplication.\\~\\

\subsection{Explanation for variables in SymbolicCones Struct}

In our implementation, SymbolicCone struct has some properties such as generators, apex, openness, dimension and ambient\_dimension where generators are the generators of the cone, apex's are vector of rationals, openness of the cone indicates which faces of the cone are open, dimension shows dimension of the cone and ambient\_dimension shows dimension of the ambient space.

\subsection{List of functions for SymbolicCone}

\begin{itemize}

    \item flip(s) : Takes symbolic cone and flip the signs.
    \item generators\_matrix(s) : Takes symbolic cone and returns matrix with generators as columns.
    \item macmahon\_cone(A,b) : Takes A matrix with integers and b for equalities and returns macmahon cone Symbolic cone.
    \item enumerate\_fundamental\_parallelepiped(s) : Takes a symbolic cone and returns list of integer points in the fundamental parallelepiped of given symbolic cone.
    \item eliminate\_last\_coordinate(s, e) : Takes symbolic cone and equality parameters and returns Brion Decomposition of self intersected with the half-space in which last coordinate is non-negative.
\end{itemize}

\section{CombinationOfCones Struct}

\subsection{Explanation for variables in SymbolicCones Struct}

In this struct, there is a dictionary where SymbolicCone variables are kept and functions where these variables are processed. 

\subsection{List of methods for CombinationOfCones}

\begin{itemize}
    \item add\_cone(s, o, m) : Takes CombinationOfCones itself, other cone and a multiplicity and adds two SymbolicCones together with multiplicity.
    \item map\_cones(s, f) : Takes a CombinationOfCones itself and map a function to all of its elements.
    \item sumq(g, f) : Takes vector of SymbolicCone's and returns their sum as CombinationOfCones variable. If initial CombinationOfCones variable wanted to be given, f variable is available for it.
    
\end{itemize}

\section{LinearDiophantineSystem Struct}

A linear Diophantine system can be represented mathematically as:\\

Ax = b (mod E)\\

where x is a vector of variables, A is a matrix of coefficients, b is a vector of constants, and E is a vector of moduli.\\

\subsection{List of variables and functions of LinearDiophantineSystem}

In Julia, the system can be represented by a struct with elements such as:

\begin{itemize}
    \item A: a matrix of coefficients
    \item b: a vector of constants
    \item E: a vector of moduli
    \item cone: a SymbolicCone struct variable, which represents the cone of the solution space
    \item \_symbolic\_cones: a variable that holds CombinationOfCones, which represents the symbolic cones of the solution space
    \item \_fundamental\_parallelepipeds: a variable that holds the fundamental parallelepipeds calculated for the system
    \item \_rational\_function: a variable that holds the rational function of the system, which can be used to represent the solution space.
\end{itemize}

and functions defined for that struct are defined as below:

\begin{itemize}
    \item symbolic\_cones(s,l) : Takes a LinearDiophantineSystem and computes representation of the set of all non-negative integral solutions as linear combination of SymbolicCones. If user would like to see the logs, l parameter takes a boolean that opens and closes that option.
    \item fundamental\_parallelepipeds(s) : Takes a LinearDiophantineSystem and calculates fundamental parallelepipeds of every cone defined in \_symbolic\_cones variable. 
    \item rational\_fundtion\_string(s) : Takes a LinearDiophantineSystem and computes string representation of rational generating function of given system.
\end{itemize}
\newpage
\section{Screenshots}

Here are some screenshots for Polyhedral Omega Julia implementation and comparison of Sage implementation time difference:

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=1\textwidth]{Imgs/screenshots/msv_python.png}
    \caption{\label{fig:msv_python}msv function Sage implementation}
\end{figure}

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=1\textwidth]{Imgs/screenshots/msv_julia.png}
    \caption{\label{fig:msv_julia}msv function Julia implementation}
\end{figure}

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=1\textwidth]{Imgs/screenshots/svv_python.png}
    \caption{\label{fig:svv_python}svv function Sage implementation}
\end{figure}

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=1\textwidth]{Imgs/screenshots/svv_julia.png}
    \caption{\label{fig:svv_julia}svv function Julia implementation}
\end{figure}\textbf{}

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=1\textwidth]{Imgs/screenshots/intify_v_python.png}
    \caption{\label{fig:intify_v_python}posmult function Julia implementation}
\end{figure}\textbf{}

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=0.8\textwidth]{Imgs/screenshots/intify_v_julia.png}
    \caption{\label{fig:intify_v_julia}posmult function Julia implementation}
\end{figure}\textbf{}

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=1\textwidth]{Imgs/screenshots/is_integral_python.png}
    \caption{\label{fig:is_integral_python}is\_integral function Sage implementation}
\end{figure}\textbf{}

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=1\textwidth]{Imgs/screenshots/is_integral_julia.png}
    \caption{\label{fig:is_integral_julia}is\_integral function Julia implementation}
\end{figure}\textbf{}

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=1\textwidth]{Imgs/screenshots/smithnormalform_python_time.png}
    \caption{\label{fig:is_integral_julia}smith function Julia implementation}
\end{figure}\textbf{}

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=1\textwidth]{Imgs/screenshots/smithnormalform_julia,_time.png}
    \caption{\label{fig:is_integral_julia}smith function Julia implementation}
\end{figure}\textbf{}

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=1\textwidth]{Imgs/screenshots/symbolic_cones.png}
    \caption{\label{fig:is_integral_julia}symbolic\_cones function Julia implementation}
\end{figure}\textbf{}


\begin{figure}[!htbp]
    \centering
    \includegraphics[width=1\textwidth]{Imgs/screenshots/fundamental_parallelepiped.png}
    \caption{\label{fig:is_integral_julia}fundamental\_parallelepiped function Julia implementation}
\end{figure}\textbf{}

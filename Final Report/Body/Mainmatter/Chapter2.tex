\chapter{POLYHEDRAL OMEGA ALGORITHM}

\section{Elliott}

Eliott was the first to develop such a calculus that yielded an algorithmic method for solving a linear Diophantine equation. At the same time, MacMahon was pursuing
the ambitious and visionary project of applying such methods to partition theory. While algorithmic, Elliott’s method was not practical for MacMahon’s purposes, as it was too computationally intensive to carry out by hand. Therefore, MacMahon developed and continually extended his own calculus which enabled him to solve many partition theoretic problems, even though it did not constitute a general algorithm. Despite many successes, MacMahon did not achieve the goal he had originally set himself of using his partition analysis calculus to prove his famous formula for plane partitions.

\begin{figure}[!htbp]
\centering
\begin{subfigure}{.24\textwidth}
    \centering
    \includegraphics[width=1\textwidth]{Imgs/Elliott/1.png}
    \caption{\label{fig:elliott:1}$l_\mathbb{R} ((1,0,1), (0,1,-3))$}
\end{subfigure}%
\begin{subfigure}{.24\textwidth}
    \centering
    \includegraphics[width=1\textwidth]{Imgs/Elliott/2.png}
    \caption{\label{fig:elliott:2}$v = (1,1,-2)$}
\end{subfigure}
\begin{subfigure}{.24\textwidth}
    \centering
    \includegraphics[width=1\textwidth]{Imgs/Elliott/3.png}
    \caption{\label{fig:elliott:3}Drop green cone}
\end{subfigure}%
\begin{subfigure}{.24\textwidth}
    \centering
    \includegraphics[width=1\textwidth]{Imgs/Elliott/4.png}
    \caption{\label{fig:elliott:4}$l_\mathbb{R}((1,0,1), (1, 1, -2) \\v = (2, 1, -1)$}
\end{subfigure}
\begin{subfigure}{.24\textwidth}
    \centering
    \includegraphics[width=1\textwidth]{Imgs/Elliott/5.png}
    \caption{\label{fig:elliott:5}Drop green cone}
\end{subfigure}%
\begin{subfigure}{.27\textwidth}
    \centering
    \includegraphics[width=1\textwidth]{Imgs/Elliott/6.png}
    \caption{\label{fig:elliott:6}$l_\mathbb{R}((1,0,1), (2, 1, -1))\\ v = (3, 1, 0)$}
\end{subfigure}
\begin{subfigure}{.24\textwidth}
    \centering
    \includegraphics[width=1\textwidth]{Imgs/Elliott/7.png}
    \caption{\label{fig:elliott:7}Drop green cone}
\end{subfigure}%
\begin{subfigure}{.24\textwidth}
    \centering
    \includegraphics[width=1\textwidth]{Imgs/Elliott/8.png}
    \caption{\label{fig:elliott:8}$l_\mathbb{R}((1,0,1), (3, 1, 0))$}
\end{subfigure}
\caption[Elliott's decomposition method]{Elliott's decomposition method\cite{polyhedral_omega}}
\end{figure}

The Polyhedral Omega algorithm consists of the following steps:
\section{McMahon Lifting}

MacMahon lifting is used to transform the representation of the problem. For given $A \in \mathbb{Z}^{mxd}$ and $b \in \mathbb{Z}^m$, we construct a matrix $V \in \mathbb{Z}^{(d+m)xd}$ and a vector $q \in \mathbb{Z}^{d+m}$ by

\begin{center}
    $V = {I d_d \choose A}$ and $q = {0 \choose -b}$
\end{center}

This is a definition of a cone with $C = \mathbb{C}(V;q)$ with apex q and the columns of V as generators.

\begin{algorithm}
\caption{MacMahon Lifting}
\label{MacMahon Lifting}
\begin{algorithmic}
\Require $A$ matrix $A \in \mathbb{Z}^{mxd}$ and vector $b \in \mathbb{Z}^m$ % i.e. input
\Ensure A symbolic cone C = (V, q, o) with $V \in \mathbb{Z}^{(d + m)xd}$, $q \in \mathbb{Z}^{d + m}$ and $o \in \{0, 1\}^d$ s.t. $\Omega_\geq^m (C) = \{ z \in \mathbb{R}_\geq^d | Az \geq b \}$ % i.e. output
\Function {macmahon}{A,b}
  \State $V = {I d_d \choose A}$
  \State $q = {0 \choose -b}$
  \State $o = 0 \in \{0, 1\}^d$
  \State \Return (V,q,o)
\EndFunction
\end{algorithmic}
\end{algorithm}

\section{Iterative Elimination of the Last Coordinate using Symbolic Cones}

\subsection{Eliminate Last Coordinate}

A \emph{Symbolic Cone} C is simply a triple (V,q,o) where V is a matrix of generators, q is the apex of the cone and o is a vector indicating which faces of C are open.This is the form in which we are going to store cones throughout the algorithm.  \\
\newpage

\begin{algorithm}
\caption{Eliminate Last Coordinate}
\label{Eliminate Last Coordinate}
\begin{algorithmic}
\Require Symbolic Cone C = (V,q,o) with $V\in\mathbb{Z}^{nxk}$, $q \in \mathbb{Q}^n$ and $o \in \{0,1\}^k$, such that projection $\pi$ that forgets the last coordinate induces a bijection between aff(C) and $\mathbb{R}^{n-1}$ % i.e. input
\Ensure A linear combination $\sum_i\alpha_iC_i$ of symbolic cones, represented as a dictionary mapping cones $C_i = (V_i, q_i,o_i)$ which we called CombinationOfCones struct to multiplicates $\alpha_i$ s.t. $\Omega_\geq ([C]) = \sum_i\alpha_i[C_i]$  % i.e. output
\Function{eliminate\_last\_coordinate}{C}
    \State $v_j$ = the jth column of V inside C
    \State $w_j = q - \ffrac{q_n}{v_{j,n}}v_j$
    \State $J^+ = \{j \in [k] | v_{j,n} > 0\}$
    \State $J^- = \{j \in [k] | v_{j,n} < 0\}$
    \State $o'(j) = (o'(j)_i)_{i=1,...,k}$ where $o'(j)_i = o_i$ if $i \neq j$ else 0
    \State \[
T^j = \begin{bmatrix} 
    -V_{j,n} & \dots  \\
    \vdots & \ddots \\
     &  -V_{j,n} \\
    V_{1,n} & V_{j-1, n}  & -1 & V_{j+1,n} & \dots & V_{k,n}\\
    &  & &-V_{j,n} \\
      &    &  & & &-V_{j,n}
    \end{bmatrix}
\]
    \State $G^j = prim(\pi((sg(q_n)\cdot V\cdot T^j))$
    \State $B^+ = [(G^j, \pi(w_j), o'(j)) for j \in J^+]$
    \State $B^- = [(G^j, \pi(w_j), o'(j)) for j \in J^-] \cup [(prim(\pi(V)), \pi(q), o)]$
    \State $B^-$ if $q_n \geq 0$ else $B^+$
    \State D = the dictionary that maps flip($C'$)\ref{Flip} to sgn($C'$) for all $C' \in B$
    \State \Return D 
\EndFunction
\end{algorithmic}
\end{algorithm}

To derive these formulas, we have to distinguish three cases: Whether the apex q of C lies above the
hyperplane Hn = {x | xn = 0}, on the hyperplane or below the hyperplane. You can check how algorithm works from here.\cite{polyhedral_omega}, page 23.\newpage

\subsection{Flip}

As you can see, there is a flip function used inside \emph{eliminate\_last\_coordinate}\ref{Eliminate Last Coordinate} function. implementation shown below,


\begin{algorithm}
\caption{Flip}
\label{Flip}
\begin{algorithmic}
\Require A symbolic cone C = (V, q, o) with $V \in \mathbb{Z}^{dxk}$, $q\in\mathbb{Q}^d$ and $o\in\{0,1\}^k$ % i.e. input
\Ensure A pair (s,C')  of a symbolic cone C' and a sign s such that $[C] \equiv s[C']$ modulo lines and s = sgn(C). % i.e. output
\Function{flip}{C}
    \State $f(i)$ = the ith column of V is backward
    \State $\sigma (i) = 1$ if $f(i)$ else -1
    \State $s = \prod_{i=1}^k\sigma(i)$
    \State $V' = $ the matrix obtained from V by multiplying the ith column by $\sigma(i)$
    \State $o' = $ the vector defined by $o_i' = xor(o_i, f(i))$ for $i = 1,...,k$
    \State $C' = (V', q, o')$
    \State \Return $(s, C')$
\EndFunction
\end{algorithmic}
\end{algorithm}

You can check how algorithm works from here.\cite{polyhedral_omega}, page 22.


\subsection{Eliminate Coordinates}

We have seen that \emph{eliminate\_last\_coordinate}\ref{Eliminate Last Coordinate} function applies $\Omega_\geq$ operator correctly to apply linear combination of symbolic cones of the form of MacMahon. This is a haşper function that iterates elimination of the last variable $m$ times. 


\begin{algorithm}
\caption{Eliminate Coordinates}
\label{Eliminate Coordinates}
\begin{algorithmic}
\Require  symbolic cone C = (V, q, o) with $V \in \mathbb{Z}^{dxk}$, $q\in\mathbb{Q}^d$ and $o\in\{0,1\}^k$ such that projection that forgets the last $m$ coordinates induces a bijection between $aff(C)$ and $\mathbb{R}^d$. % i.e. input
\Ensure  A linear combination $\sum_i\alpha_iC_i$ of symbolic cones, represented as a dictionary mapping cones $C_i = (V_i, q_i,o_i)$ which we called CombinationOfCones struct to multiplicates $\alpha_i$ s.t. $\Omega_\geq ([C]) = \sum_i\alpha_i[C_i]$  % i.e. output
\Function{symbolic\_cones}{C}
    \For{$i = 1,...,m$}
    \State $C \gets map(eliminate\_last\_coordinate, C)$
    \EndFor
    \State \Return C
\EndFunction
\end{algorithmic}
\end{algorithm}

The elimination algorithm is easy to implement, since all cones are stored in terms of integer matrices and all transformations are given explicitly. 

\subsection{Primitive}
it is important to ensure that the generators for a given cone $C_i$ are stored in primitive form. Algorithm \ref{Prim} shows how to compute $prim(v)$ for integer vectors.

\begin{algorithm}
\caption{Make a vector primitive}
\label{Prim}
\begin{algorithmic}
\Require  A non-zero integer vector $v \in \mathbb{Z}^n$ % i.e. input
\Ensure  The shortest integer vector that is a positive muliple of $v$  % i.e. output
\Function{prim}{$v$}
    \State g = $gcd(v_1, v_2, ..., v_n)$
    \State \Return $\ffrac{v}{|g|}$
\EndFunction
\end{algorithmic}
\end{algorithm}

After each round of elimination, a given cone $C_i$ may appear multiple times in the sum with multiplicities of opposite signs so we have to collect terms after each elimination and sum them up the multiplicities of all instances of a given cone $C_i$.

\subsection{Map Linear Combination Of Symbolic Cones}

the generators of the cones $C$ should be stored in (lexicographically) sorted order, since when generators are primitive and sorted, every rational simplicial cone has a \emph{unique} representation as a symbolic cone. Also, it may be appropriate to store the sum as a dictionary mapping symbolic cones $C_i$ to multiplicities $\alpha_i$ throughout the computation, see the definition of map\_cones function \ref{Map_cones}

\begin{algorithm}
\caption{Map Linear Combination Of Symbolic Cones}
\label{Map_cones}
\begin{algorithmic}
\Require  A function $f$ which maps a symbolic cones to a linear combination of cones, and a linear combination $\sum_i{a_iC_i}$ of symbolic cones, represented as a dictionary D mapping symbolic cones $C_i$ to multiplicities $\alpha_i$.  % i.e. input
\Ensure  A dictionary representing the linear combination $\sum_i\alpha_i f(C_i) = \sum_{i,j}\alpha_i \beta_{i,j} C_{i,j}$ of symbolic cones where $f(C_i) = \sum_i{\beta_{i,j}C_{i,j}}$. Coefficients are collected by the $C_{i,j}$.  % i.e. output
\Function{map\_cones}{$D, f$}
    \State L = an empty dictionary with default value 0
    \For{$(C \rightarrow \alpha ) \in D $}
        \State $D' = f(C)$
        \For{$(C' \rightarrow \beta) \in D'$}
            \State $L[C'] \leftarrow L[C'] + (\alpha \cdot \beta)$        
        \EndFor
    \EndFor
    \State \Return L 
\EndFunction
\end{algorithmic}
\end{algorithm}

\section{Conversion to Rational Functions}

Rational function expression for $\Phi_p$ is required so there are two methods for this which are explicitly enumerating the lattice points in the fundamental parallelepiped of a cone that uses an explicit formula based on the Smith Normal Form of the matrix of generators and using Barvinok's recursive decomposition to express a given cone as a short signed sum of unimodular cones. In our implementation, Smith normal form matrix generation formula used. 

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=1\textwidth]{Imgs/SmithNormalForm/image.png}
    \caption[Smith Normal Form]{\label{fig:pol_omeg}The Smith normal form can be viewed as aligning the bases of $\mathbb{Z}^n$ and a sublattice $L$ so that the fundamental parallelepiped of the sublattice is a rectangle with respect to the chosen bases.\cite{polyhedral_omega}}
\end{figure}

\subsection{Enumerate Fundamental Parallelepiped}

\emph{Fundamental Parallelepiped Enumeration} is done as,\\~\\

$\rho_C(Z) = \ffrac{\sum_{v\in \mathbb{Z}^n \cap \prod^0(v_1, v_2, ..., v_d;q)} z^v }{(1-z^{v_1})\cdot(1-z^{v_2})\cdot...\cdot(1-z^{v_d})}$
\\~\\
where $v_i$ are generators of $C$ and  $\prod^0(v_1, v_2, ..., v_d;q)$ is the fundamental parallelepiped of $C$ with given openness $o$ and apex $q$.
\newpage
\begin{algorithm}
\caption{Enumerate Fundamental Parallelepiped}
\label{Enumerate Fundamental Parallelepiped}
\begin{algorithmic}
\Require  A symbolic cone $C = (V,q,o)$ with $V \in \mathbb{Z}^{dxk}$, $q \in  \mathbb{Q}^d$ and $o \in \{0,1\}^k$  % i.e. input
\Ensure The list of all lattice points in the fundamental parallelepiped of C   % i.e. output
\Function{enumerate\_fundamental\_parallelepiped}{$C$}
    \If{aff$(C)$ does not contain a lattice point}
        \State \Return []
    \Else 
        \State $p = a$ lattice point in aff $(C)$
        \State U, S, W = smith(V)
        \State $s_1, ..., s_k$ = diagonal elements of S
        \State $s_i'$ = $\ffrac{s_k}{s_i}$ for all $i = 1,...,k$
        \State $S' = $ the $(k x n)$ matrix with diagonal entries $s_i'$
        \State $\hat{q}$ = $U^{-1}(q - p)$
        \State $\Tilde{q}^{int} = \lfloor -W^{-1} S' \hat{q} \rfloor$
        \State $\Tilde{q}^{frac} = -W^{-1}S'\hat{q} - \Tilde{q}^{int}$
        \State $o' = (o_j')_{j = 1,...,k}$ where $o_j' = o_j$ if $\Tilde{q}^{frac}_j = 0$ else 0
        \State $P = \{(x_1, ..., x_k) | x_i = 0, ..., s_i - 1$ for all $i = 1,...,k\}$
        \State $L = [\ffrac{1}{s_k}(V((W^{-1}S'x +\Tilde{q}^{int}) mod^{o'} s_k ) + V\Tilde{q}^{frac} + s_kq)$ for $x\in P]$
        \State \Return $L$
    \EndIf
\EndFunction
\end{algorithmic}
\end{algorithm}

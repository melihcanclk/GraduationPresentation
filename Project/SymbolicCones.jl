#mutable struct SymbolicCone
using LinearAlgebra;
using IterTools;
import Base: *

include("SmithNormalForm.jl")
include("helpers.jl")

"""The vertex description of a simplicial symbolic cone.
    The vertex decription of a simplicial cone consists of its apex (which may be rational), the matrix of generators (which must be integral and linearly independent) and a vector of boolean flags indicating which faces are open and closed.
    SymbolicCones are immutable and hashable to facilitate using them as keys in a dictionary.
    This class implements intersecting a SymbolicCone with a coordinate half-space, enumerating the fundamental parallelepiped of a SymbolicCone, computing an inequality description of the symbolic cone and flipping a cone "forward".
    Attributes:
        generators: The generators of the cone, given as a vector of integer vectors. Generators must be linearly independent. Read-only.
        apex: The apex of the cone, given as a vector of rationals or vector of integers. Read-only.
        openness: A vector of booleans, indicating which faces of the cone are open. 
                If openness[i] == 1, the coefficient of the i-th generator is strictly bigger than one, so that the opposite face is open. Conversely, if penness[i] == 0, the coefficient of the i-th generator is strictly bigger than one, so that the opposite face is open.
        dimension: Dimension of the cone, that is, the number of generators. Read-only.
        ambient_dimension: The dimension of the ambient space in which the cone lies, that is, the length of each generator. Read-only.
    """
mutable struct SymbolicCone
    _generators::Vector{Vector{Int64}}
    _apex::Union{Vector{Int64},Vector{Rational{Int64}}}
    _openness::Vector{Bool}
    _dimension::Int64
    _ambient_dimension::Int64


    #initialize SymbolicCone struct (constructor) 
    function SymbolicCone(generators::Vector{Vector{Int64}}, apex::Union{Vector{Int64},Vector{Rational{Int64}}}, openness::Vector{Bool}, sort::Bool)
        # new instance of SymbolicCone
        if (sort)
            #sort generators
            generators, openness = sort_tuplized(generators, openness)
        end
        cone = new(generators, apex, openness, length(generators), length(generators[1]))
        return cone
    end

    #initialize SymbolicCone struct (constructor) 
    function SymbolicCone(generators::Vector{Vector{Int64}}, apex::Union{Vector{Int64},Vector{Rational{Int64}}}, openness::Vector{Bool})
        # new instance of SymbolicCone
        generators, openness = sort_tuplized(generators, openness)
        cone = new(generators, apex, openness, length(generators), length(generators[1]))
        return cone
    end

    #constructor without openness
    function SymbolicCone(generators::Vector{Vector{Int64}}, apex::Union{Vector{Int64},Vector{Rational{Int64}}}, sort::Bool)
        #false for all openness
        if (sort)
            #sort generators
            generators, openness = sort_tuplized(generators, openness)
        end
        openness = [false for i in 1:length(generators)]
        return new(generators, apex, openness, length(generators), length(generators[1]))
    end

    #constructor without openness
    function SymbolicCone(generators::Vector{Vector{Int64}}, apex::Union{Vector{Int64},Vector{Rational{Int64}}})
        #false for all openness
        generators, openness = sort_tuplized(generators, openness)
        return new(generators, apex, openness, length(generators), length(generators[1]))
    end

    """Two cones are equal if they have equal generators, apex and openness."""
    function Base.:(==)(self::SymbolicCone, other::SymbolicCone)
        return self._generators == other._generators && self._apex == other._apex && self._openness == other._openness
    end

    """
        Multiplication of SymbolicCone with an integer returns a CombinationOfCones.
        Arguments:
            i: An integer, represented as an int or a Sage Integer.
    """
    function Base.:*(self::SymbolicCone, i::Int64)
        C = CombinationOfCones(Dict(self => i))
        return C
    end

    """
    use the * operator to multiply a SymbolicCone with an integer
    """

    function Base.:*(i::Int64, self::SymbolicCone)
        return self * i
    end

    """
    If we add twot SymbolicCone objects, we get a CombinationOfCones object return 
    """
    function Base.:+(self::SymbolicCone, other::SymbolicCone)
        if self == other
            C = CombinationOfCones(Dict(self => 2))
        else
            C = CombinationOfCones(Dict(self => 1, other => 1))
        end
        return C
    end

    """
        "[ Cone: generators = %s, apex = %s, openness = %s ]" % (self.generators.__repr__(), self.apex.__repr__(), self.openness.__repr__())
    """

    function Base.show(io::IO, self::SymbolicCone)
        print(io, "[ Cone: generators = ", self._generators, ", apex = ", self._apex, ", openness = ", self._openness, " ]")
    end
end

"""Computes hash value of self."""
function hash(self::SymbolicCone)
    return hash(self._generators, self._apex, self._openness)
end

"""
    # we have to use lists instead of generators here, because zip does not work for generators
"""
function flip(self::SymbolicCone)
    J = [!vector_is_forward(v) for v in self._generators] # 1 = backward, 0 = forward
    # Jpm = [ -_1 if Ji else _1 for Ji in J ] # -1 = backward, 1 = forward
    Jpm = [Ji ? -1 : 1 for Ji in J] # -1 = backward, 1 = forward
    # # s = (-1)**(number of backward generators)
    s = (-1)^(sum(J))
    _o = [oi != Ji for (oi, Ji) in zip(self._openness, J)]
    _V = [msv(Jpmi, v) for (Jpmi, v) in zip(Jpm, self._generators)]
    return s * SymbolicCone(_V, self._apex, convert(Vector{Bool}, _o))
end

function generators_matrix(self::SymbolicCone)
    """Returns a matrix with generators as columns."""
    return Matrix(hcat(self._generators...))
end

function smith_normal_form(A::Matrix{Int64})
    # get smith normal form 
    F = smith(A)
    return diagm(F), F.T, F.S
end

# """Takes a  matrix A and a  vector b and solves the system Ax = b over the integers.

#     Returns a solution as a  integer vector.

#     Arguments:
#         A: An integer matrix, given as a object.
#         b: An integer vector, given as a object.
#     """
# function solve_linear_equation_system_over_integers(A, b)
#     # get smith normal form 
#     S, Vinv, Uinv = smith_normal_form(A)

#     n = size(A)[1]
#     m = size(A)[2]
#     #invert Vinv and Uinv 
#     Vinv = inv(Vinv)
#     Uinv = inv(Uinv)

#     p = Uinv * b
#     q = vcat([p[i] / S[i, i] for i in 1:m], [0 for i in 1:n-m])
#     x = Vinv * q
#     return x
# end

#create function that calculates all generators of a cone given matrix A and vector b
function identity_matrix_with_A(A::Matrix{Int64}, d::Int64)
    I = Matrix(Diagonal(ones(Int64, d)))
    combined_matrix = vcat(I, A)
    c_vectors = eachcol(combined_matrix)
    # iterate over c_vectors and push each variable to vector
    generator = Vector{Vector{Int64}}()
    for c_vector in c_vectors
        push!(generator, c_vector)
    end
    return generator
end

function macmahon_cone(A, b)
    # d is ncols of A
    d = size(A, 2)
    generators = identity_matrix_with_A(A, d)
    x = [-1 * bi for bi in b]
    y = fill(0, d)
    apex = append!(y, x)
    # get apex vector,
    openness = [false for i in 1:d]
    return SymbolicCone(generators, apex, openness)
end

"""Returns a list of all integer points in the fundamental parallelepiped of self.

The integer points in the list are represented as Int64. 
 Note that this list may be exponentially large; its computation may therefore take a
  long time and consume a lot of memory.
"""
function enumerate_fundamental_parallelepiped(self::SymbolicCone)
    k = self._dimension
    d = self._ambient_dimension
    V = generators_matrix(self)
    q = self._apex
    p = [0 for i in 1:d]
    S, Winv, Uinv = smith_normal_form(V)
    Uinv = inv(Uinv)
    Winv = inv(Winv)
    s = [S[i, i] for i in 1:k]
    sk = s[end]
    sprime = [convert(Int64, (sk / si)) for si in s]
    qhat = Uinv * (q - p)
    Wprime = [Winv[i, 1:k] .* sprime[1:k] for i in 1:k]
    qtrans = [-sum(Wprime[j][i] * qhat[i] for i in 1:k) for j in 1:k]
    qfract = fract_simple(qtrans)
    qint = [floor(qi) for qi in qtrans]
    multpct = sk * q + V * Vector(qfract)
    qsummand = [round(Int64, qi) for qi in multpct]
    o = [qfract[i] == 0 ? self._openness[i] : 0 for i in 1:k]
    ranges = [1:s[i] for i in 1:k]
    P = collect(product(ranges...))
    #substract all entries of P by 1
    P = [p .- 1 for p in P]
    """
    transform_integral
    """
    function _transform_integral(z)
        # substract all entries of z by 1
        innerRes = []
        j = 1
        for qj in qint # qint has k entries
            inner = 0
            i = 1
            for zi in z # z should have k entries
                inner = inner + (Wprime[j][i] * zi) # Wprime[i,j] * vj
                i += 1
            end
            inner += qj
            inner = mod(inner, sk)
            # inner has to be modified according to o

            if inner == 0 && (o[j] == 1)
                inner = sk
            end
            push!(innerRes, inner)
            j += 1
        end
        outerRes = []
        for l in 1:d
            outer = 0
            j = 1
            for innerResi in innerRes
                outer = outer + (V[l, j] * innerResi)
                j += 1
            end
            push!(outerRes, outer) # outerRes is an integral vector
        end
        return [convert(Int64, div(ai + bi, sk)) for (ai, bi) in zip(outerRes, qsummand)]
    end
    L = [_transform_integral(v) for v in P]
    # convert matrix of vectors to vector of vectors
    return L
end


"""Eliminates the last coordinate of self by intersection.
More precisely, eliminate_last_coordinate computes a Brion decomposition of self intersected with the half-space in which the last coodinate is non-negative, and projects the result by forgetting the last coordinate. The elements of the Brion decomposition are all simplicial cones. The formula for decomposition assumes that the projection is one-to-one, i.e., the affine hull of self intersects the last-coordinate-equal-zero-hyperplane in codimension one. (This assumption holds, e.g., then the coordinate generated was introduced by MacMahon-lifting.)
Also supports intersection with the last-coordinate-equal-zero-hyperplane instead of the half-space where the last coordinate is non-negative. To this end, the argument equality has to be set to True.
Arguments:
    equality: True if we intersect with hyperplane instead of half-space.
"""

function eliminate_last_coordinate(self::SymbolicCone, equality::Bool=false)
    V = self._generators
    q = self._apex
    o = self._openness
    k = self._dimension
    #index of last coordinate 
    n = length(V[1])
    #lambda function that returns the last coordinate of a vector 
    w = (j) -> svv(q, msv(-q[n] // (V[j][n]), V[j]))
    sgqn = q[n] >= 0 ? 1 : -1

    function g(i, j)
        if (i == j)
            return q[n] >= 0 ? msv(-1, V[j]) : V[j]
        else
            return msv(sgqn, svv(msv(V[i][n], V[j]), msv(-V[j][n], V[i])))
        end
    end
    if equality
        G = (j) -> prim([g(i, j)[1:end-1] for i in 1:k if i != j])
    else
        G = (j) -> prim([g(i, j)[1:end-1] for i in 1:k])
    end
    function _o(j)
        if equality
            return append!(o[1:j], o[j+1:end])
        else
            return convert(Vector{Bool}, vcat(o[1:j-1], [0], o[j+1:end]))
        end
    end
    function Bplus()
        return sumq([SymbolicCone(G(j), w(j)[1:end-1], _o(j)) for j in 1:k if V[j][n] > 0])
    end

    function Bminus()
        return sumq([SymbolicCone(G(j), w(j)[1:end-1], _o(j)) for j in 1:k if V[j][n] < 0])
    end

    function Cprime()
        return CombinationOfCones(SymbolicCone(prim([v[1:end-1] for v in V]), q[1:end-1], o))
    end

    # len([Vj for Vj in V if Vj[n] > 0])
    n_up = sum([V[j][n] > 0 for j in 1:k])
    # len([Vj for Vj in V if Vj[n] < 0])
    n_down = sum([V[j][n] < 0 for j in 1:k])
    if (!equality)
        if q[n] < 0 || (q[n] == 0 && n_up <= n_down && n_up >= 1)
            B = Bplus()
        else
            B = Bminus() + Cprime()
        end
    else
        if q[n] < 0 || (q[n] == 0 && any([V[j][n] > 0 for j in 1:k]))
            B = Bplus()
        elseif q[n] > 0 || (q[n] == 0 && any([V[j][n] < 0 for j in 1:k]))
            B = Bminus()
        else
            B = Cprime()
        end
    end
    result = map_cones(B, C -> flip(C))
    return result
end


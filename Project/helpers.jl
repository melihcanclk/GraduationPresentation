using LinearAlgebra;

#Multiply all entries of the vector v with the scalar s.
function msv(s, v)
    return [s * v[i] for i in 1:length(v)]
end

"""Sum two vectors v and w."""
function svv(v, w)
    return [v[i] + w[i] for i in 1:length(v)]
end

"""Checks if a vector is forward, i.e., first non-zero coordinate is positive."""
function vector_is_forward(v)
    for i in 1:length(v)
        if v[i] != 0
            return v[i] > 0
        end
    end
    return true
end


"""Takes a rational vector v and returns a positive multiple of v that is integer.

    If the smallest possible multiple of v that is integer is required, apply prim_v afterwards. Return value is a Sage tuple of Integers."""
function posmult(v)
    denominators = [denominator(v[i]) for i in 1:length(v)]
    lcm = prod(denominators)
    return [lcm * v[i] for i in 1:length(v)]
end


"""Check if a vector v is integral."""
function is_integral(v)
    return all([isinteger(vi) for vi in v])
end

function gcd_vec(v::Vector{Int})
    return reduce(gcd,v)
end

# Takes an integer vector v and returns the corresponding primitive integer vector.

# The primitive integer vector corresponding to v is the least positive multiple of v that is still an integer vector.

# Arguments:
# v: An integer vector, represented as a tuple of Sage Integers.

function prim_v(v::Vector{Int})
    d = abs(gcd_vec(v))
    if d == 1
        return v
    else
        return map(x->x÷d, v)
    end
end

"""Takes a tuple of integer vectors and applies prim_v to each of them."""
function prim(v_list::Vector{Vector{Int64}})
    return [prim_v(v) for v in v_list]
end

""" 
takes a vector and returns only fractional part of each entry
"""
function fract_simple(v::Vector{Float64})
    return [vi - floor(vi) for vi in v]
end



function sort_tuplized(inputs...)
    return map(collect,zip(sort(collect(zip(inputs...)); by=first)...))
end

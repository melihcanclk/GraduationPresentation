include("SymbolicCones.jl")

mutable struct CombinationOfCones
    """
    A Dict of SymbolicCones indexed by Int64 values
    """
    cones::Dict{SymbolicCone,Int64}

    function CombinationOfCones(cone::SymbolicCone, multiplicity::Int64=1)
        # initialize empty Dict
        cones = Dict{SymbolicCone,Int64}()
        cones[cone] = multiplicity
        return new(cones)
    end

    function CombinationOfCones(cones::Dict{SymbolicCone,Int64})
        return new(cones)
    end

    function CombinationOfCones()
        # initialize empty Dict
        return new(Dict{SymbolicCone,Int64}())
    end

    """Returns a new CombinationOfCones that is the sum of self and another SymbolicCone or CombinationOfCones."""
    function Base.:+(self::CombinationOfCones, other::Union{SymbolicCone,CombinationOfCones})
        if other isa SymbolicCone
            C = CombinationOfCones(self.cones)
            add_cone(C, other)
            return C
        elseif other isa CombinationOfCones
            C = CombinationOfCones(self.cones)
            for (cone, multiplicity) in other.cones
                add_cone(C, cone, multiplicity)
            end
            return C
        end
    end

    """Returns a new CombinationOfCones that is self multiplied with a scalar factor given by other."""
    function Base.:*(self::CombinationOfCones, other::Int64)
        C = CombinationOfCones()
        for cone in keys(self.cones)
            C.cones[cone] = self.cones[cone] * other
        end
        return C
    end

    function Base.show(io::IO, self::CombinationOfCones)
        # print all symbolic cones in self every new line 
        print(io, "Combination of cones: \n")
        for (cone, multiplicity) in self.cones
            print(io, "[", cone, "]^", multiplicity, "\n")
        end
    end

end

"""
    Adds cone to this linear combination of cones.

    Self is modified in place. Multiplicity of added cone may be given. Returns self.

    Arguments:
        cone: The SymbolicCone to be added to self.
        multiplicity: The multiplicity of cone, given as an Integer.
       
"""

function add_cone(self::CombinationOfCones, cone::SymbolicCone, multiplicity::Int64=1)
    if multiplicity != 0
        if cone in keys(self.cones)
            old_m = self.cones[cone]
        else
            old_m = 0
        end
        new_m = old_m + multiplicity
        if new_m == 0 && cone in keys(self.cones)
            delete!(self.cones, cone)
        else
            self.cones[cone] = new_m
        end
    end
    return self
end

"""
   Applies function f to every cone in self and returns the linear combination of results.

        f is a function taking a SymbolicCone and returning a CombinationOfCones. If self represents a combination of cones a_1 * c_1 + ... + a_k * c_k, then self.map_cones(f) returns the combination a_1 * f(c_1) + ... + a_k * f(c_k). If f(c_i) is itself a linear combination, then the distributive law is applied to flatten the sum and collect terms. map_cones not modify self, but instead returns a new CombinationOfCones instance.

        Arguments:
            f: A function of a single argument, mapping a SymbolicCone to a CombinationOfCones.
   
       
"""
function map_cones(self::CombinationOfCones, f::Function)
    result = CombinationOfCones()
    for (cone, multiplicity) in self.cones
        combination = f(cone)
        combination *= multiplicity
        result += combination
    end
    return result
end

"""
    Takes vector of SymbolicCones  and returns their sum.

    Arguments:
        gen: A list or generator of SymbolicCones and CombinationOfCones. Both types can be mixed.
        first: An initial CombinationOfCones to which all elements in gen are added. First is modified in-place. If first is omitted a new empty CombinationOfCones is created.
"""
function sumq(gen::Vector{SymbolicCone}, first::CombinationOfCones=CombinationOfCones())
    result = first
    for c in gen
        result += c
    end
    return result
end
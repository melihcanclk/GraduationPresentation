include("SymbolicCones.jl")
include("CombinationOfCones.jl")
include("helpers.jl")

mutable struct LinearDiophantineSystem
    """Creates a new LinearDiophantineSystem Ax >=^E b, x >= 0 for a given matrix A, a right-hand side b and 
        a list E of flags indicating whether the corresponding row is an inequality or an equation. 
        
        The class LinearDiophantineSystem can be used to compute symbolic cone and/or rational function
        representations of the set of all non-negative integral solutions to the given linear system. 

        The parameters A,b,E cannot be modified once the LinearDiophantineSystem has been constructed. 
        The methods for solving the system, symbolic_ones, fundamental_parallelepipeds, and rational_function_string, 
        each cache the results of their computations so that they can be run several times and the computation is done 
        only once.
        
        Arguments:
            A: An integer matrix represented as a sequence of rows. Entries can be Integers.
            b: An integer vector, represented as a sequence. Entries can be Integers.
            E: A list with 0-1 entries which encodes with rows are equalities. (1 means equality, 0 means inequality.) 
                If E is None, then all constraints are assumed to be inequalities, i.e., E is constant 0.
        """
    A::Matrix{Int64}
    b::Vector{Int64}
    E::Vector{Int64}
    cone::SymbolicCone
    _symbolic_cones::Union{CombinationOfCones,Nothing}
    _fundamental_parallelepipeds
    _rational_function

    function LinearDiophantineSystem(A, b, E)
        if E == nothing
            E = zeros(Int64, size(A)[1])
        end
        cone = macmahon_cone(A, b)
        return new(A, b, E, cone, nothing, nothing, nothing)
    end
end

"""Computes a representation of the set of all non-negative integral solutions as a linear combination of symbolic cones.

Returns a CombinationOfCones representing the result.
"""
function symbolic_cones(self::LinearDiophantineSystem, logging=false)
    println("Computing symbolic cones...")
    if isnothing(self._symbolic_cones)
        E = self.E
        E = reverse(E)
        self._symbolic_cones = CombinationOfCones(self.cone, 1)
        total_time = 0.0
        for e in E
            time_spent = @elapsed begin
                self._symbolic_cones = map_cones(self._symbolic_cones, cone -> eliminate_last_coordinate(cone, Bool(e)))
            end
            if logging
                println("symbolic cones: $self._symbolic_cones")
            end
            total_time += time_spent
        end
        println("Total time for symbolic cones: $total_time seconds")
    end
    return self._symbolic_cones
end

"""
    Calculates fundamental_parallelepipeds for every cone in the symbolic_cones
    Returns a dictionary with the cones as keys and the fundamental_parallelepipeds as values
"""
function fundamental_parallelepipeds(self::LinearDiophantineSystem)
    println("Computing fundamental parallelepipeds...")
    if isnothing(self._fundamental_parallelepipeds)
        cones = symbolic_cones(self)
        self._fundamental_parallelepipeds = Dict()
        total_time = 0.0
        #traverse over dict of cones and enumerate every cone
        for (cone, value) in cones.cones
            # traverse over dict owf cones and enumerate every cone
            time_spent = @elapsed begin
                points = enumerate_fundamental_parallelepiped(cone)
            end
            total_time += time_spent
            self._fundamental_parallelepipeds[cone] = points
        end
        println("Total time for fundamental parallelepipeds: $total_time seconds")
    end
    return self._fundamental_parallelepipeds
end



"""
Computes the symbolic_cones and the fundamental_parallelepipeds and then turns this data into a string representation of the corresponding rational function.
Returns the rational function as a string of the form "r1 + r2 + ... + rn" where each ri is a rational function of the form "m * (p) / (q)" where p is a Laurent polynomial expanded with respect to the monomial basis and q is a polynomial written as a product of factors "(1-m)" where m is a multivariate monomial. Variables are "z1", ..., "zn" and exponentiation is written "z1**3", for example.

     fundamental_parallelepipeds function is not implemented yet
"""

function rational_function_string(self::LinearDiophantineSystem)
    println("Computing rational function...")
    if (isnothing(self._rational_function))
        # Matrix vector for E = 1 and Vector vector for E = 0
        function stringify_cone(cone::SymbolicCone, fundamental_parallelepiped::Union{Matrix{Vector{Int64}},Vector{Vector{Int64}},Array{Vector{Int64}}})
            d = cone._ambient_dimension
            V = cone._generators

            function stringify_monomial(z::Vector{Int64})
                #"*".join(("z%d**%d" % (i,z[i]) for i in xrange(d)))
                return join(["z" * string(i - 1) * "^" * string(z[i]) for i in range(1, d) if z[i] != 0], "*")
            end
            # apply stringify_monomial to every row of fundamental_parallelepiped
            num = join([stringify_monomial(z) for z in fundamental_parallelepiped], " + ")
            den = join(["(1-" * stringify_monomial(V[i]) * ")" for i in 1:length(V)], " * ")
            return "($num)/($den)"
        end
        #define vector 
        rational_functions = Vector{String}()
        symbolic_cone_self = symbolic_cones(self)
        total_time = 0.0
        for (cone, multiplicity) in symbolic_cone_self.cones
            _o = fundamental_parallelepipeds(self)[cone]
            time_spent = @elapsed begin
                push!(rational_functions, string(multiplicity) * "*" * stringify_cone(cone, _o))
            end
            total_time += time_spent
        end
        if length(rational_functions) > 0
            self._rational_function = join(rational_functions, " + ")
        else
            self._rational_function = "0"
        end
        println("Total time spent in rational_function_string: $total_time seconds")
    end
    return self._rational_function
end


